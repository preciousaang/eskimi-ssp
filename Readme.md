# Docmumentation for Eskimi SSP

## Step 1

Clone the repository using

> git clone https://gitlab.com/preciousaang/eskimi-ssp.git

## Step 2

In the laradock folder copy the laradock configuration using:

> cp .env.example .env

At the top, change the APP_CODE_PATH_HOST variable to your project path.

> APP_CODE_PATH_HOST=../adsManagement/

## Step 3

Go to laradock/nginx/sites. Duplicate one of the configuration using

> cp app.conf.example ads.conf

Edit the server_name and root directive to the required domain name and root folder respectively.

> server_name ads.test;<br>
> root /var/www/adsManagement/public;

## Step 4

Run the container using

> docker-compose up -d nginx mysql

## Step 5

Enter the workspace container using

> docker-compose exec workspace bash <br>

RUN

> mysql -u <enironment_user> --password=<environment_password>

Create a database using

> create database <DB_NAME>

## Step 6

While still in the workspace container, navigation to the adsManagement folder using:

> cd ../adsManagement

copy the .env.example to .env

> cp .env.example .env

Generate new encryption keys using

> php artisan key:generate

Install the required dependencies using

> composer install

Navigate to the ads-frontend folder using

> cd ../ads-frontend

Install the required frontend dependencies using

> npm install

## Step 7

> You are good to go
