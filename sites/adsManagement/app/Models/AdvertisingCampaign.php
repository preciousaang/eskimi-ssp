<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdvertisingCampaign extends Model
{
    use HasFactory;
    protected $fillable = ['to', 'from', 'daily_budget', 'total_budget', 'name', 'creative_upload'];
}
