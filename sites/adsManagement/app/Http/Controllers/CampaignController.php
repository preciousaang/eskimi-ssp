<?php

namespace App\Http\Controllers;

use App\Http\Requests\CampaignRequest;
use App\Http\Resources\CampaignResource;
use App\Models\AdvertisingCampaign;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    //
    public function create(CampaignRequest $request)
    {
        $images = [];
        $files = $request->file('images');
        foreach ($files as $file) {
            $images[] = basename($file->store('public/uploads'));
        }
        $campaign = AdvertisingCampaign::create([
            'name' => $request->post('name'),
            'from' => $request->post('from'),
            'to' => $request->post('to'),
            'daily_budget' => $request->post('dailyBudget'),
            'total_budget' => $request->post('totalBudget'),
            'creative_upload' => json_encode($images)
        ]);
        return response()->json(['campaign' => $campaign], 201);
    }

    public function list(Request $request)
    {
        $campaign = AdvertisingCampaign::latest()->get();
        $c = CampaignResource::collection($campaign);
        return response()->json(['campaigns' => $c]);
    }

    public function single($id)
    {
        $campaign = AdvertisingCampaign::findOrFail($id);
        return response()->json(['campaign' => new CampaignResource($campaign)]);
    }

    public function edit(CampaignRequest $request)
    {
        $campaign = AdvertisingCampaign::findOrFail($request->id);

        if ($request->hasFile('images')) {
            $images = [];
            $files = $request->file('images');
            foreach ($files as $file) {
                $images[] = basename($file->store('public/uploads'));
            }
            $campaign->creative_upload = json_encode($images);
        } else {
            return response()->json($request->validated());
        }

        $campaign->name = $request->post('name');

        $campaign->from = $request->post('from');
        $campaign->to = $request->post('to');
        $campaign->daily_budget = $request->post('dailyBudget');
        $campaign->total_budget = $request->post('totalBudget');


        $campaign->save();
        return response()->json(['campaign' => $campaign]);
    }
}
