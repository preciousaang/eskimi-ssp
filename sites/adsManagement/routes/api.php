<?php

use App\Http\Controllers\CampaignController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/add-campaign', [CampaignController::class, 'create']);
Route::get('list-campaigns', [CampaignController::class, 'list']);
Route::get('/campaign/{id}', [CampaignController::class, 'single']);
Route::post('/campaign/{id}/edit', [CampaignController::class, 'edit']);
