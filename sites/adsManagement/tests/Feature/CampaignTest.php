<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class CampaignTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */


    public function testCampaignCanBeAdded()
    {
        Storage::fake('my-drive');
        $file = UploadedFile::fake()->image('lara.jpg');
        $response = $this->post('/api/add-campaign', [
            'name' => 'Test',
            'from' => '2020-03-01',
            'to' => '2021-04-03',
            'dailyBudget' => '40',
            'totalBudget' => '400',
            'images' => [$file, $file, $file, $file, $file]
        ]);
        $response->assertStatus(201);
    }

    public function testCampaignAddValidationWorking()
    {
        //Delibratley ommit some values to test validation error
        Storage::fake('my-drive');
        $file = UploadedFile::fake()->image('lara.jpg');
        $response = $this->post('/api/add-campaign', [
            'name' => 'Test',
            'from' => '2020-03-01',
            'to' => '2021-04-03',
            'dailyBudget' => '',
            'totalBudget' => '',
            'images' => [$file, $file, $file, $file, $file]
        ]);
        $response->assertStatus(302);
    }
}
