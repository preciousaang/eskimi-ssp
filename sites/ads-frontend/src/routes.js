import VueRouter from "vue-router";
import Home from "./components/Home.vue";
import AddCampaign from "./components/AddCampaign.vue";
import ListCampaigns from "./components/ListCampaigns.vue";
import EditCampaign from "./components/EditCampaign.vue";

const routes = [
  { path: "/", component: Home, name: "home" },
  { path: "/add-campaign", component: AddCampaign, name: "add-campaign" },
  { path: "/campaigns", component: ListCampaigns, name: "list-campaigns" },
  { path: "/edit/:id", component: EditCampaign, name: "edit-campaign" },
];

const router = new VueRouter({ routes });

export default router;
